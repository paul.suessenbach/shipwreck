Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      get 'tile/index'
    end
  end
  namespace :api do
    namespace :v1 do
      get 'building_plan/index'
    end
  end
  namespace :api do
    namespace :v1 do
      get 'action_plan/index'
    end
  end
  # Static pages
  root 'static_pages#home'
  get '/play', to: 'static_pages#play'
  get '/about', to: 'static_pages#about'
  get '/manual', to: 'static_pages#manual'

  # Devise related routes (for users and authentication)
  devise_for :users, skip: :all
  devise_scope :user do
    post 'login', to: 'users/sessions#create'
    delete 'logout', to: 'users/sessions#destroy'
    post 'register', to: 'users/registrations#create'
  end

  # Rest API routes
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :locale, only: :index
      resources :game_session, only: %i[create index destroy]
      resources :building, only: %i[create show destroy]
      resources :status, only: %i[index show]
      resources :player_action, only: %i[create]
      resources :action_plan, only: %i[index]
      resources :building_plan, only: %i[index]
      resources :tile, only: %i[index]
    end
  end

end
