require 'test_helper'
require 'json'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:one)
  end

  test "should login for user with correct credentials" do
    post login_path, params: { user: {
        email: @user.email,
        password: 'password'
    }}
    assert_response :success
    jdata = JSON.parse response.body
    assert jdata['success']
  end

  test "should not login for user with wrong password" do
    post login_path, params: { user: {
        email: @user.email,
        password: 'foobar'
    }}
    assert_response :success
    jdata = JSON.parse response.body
    assert_not jdata['success']
    assert_not_empty jdata['errors']
  end

  test "should not login for user with unknown email" do
    post login_path, params: { user: {
        email: 'not@taken.mail',
        password: 'password'
    }}
    assert_response :success
    jdata = JSON.parse response.body
    assert_not jdata['success']
    assert_not_empty jdata['errors']
  end
end