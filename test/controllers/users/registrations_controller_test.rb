require 'test_helper'
require 'json'

class RegistrationsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:one)
  end

  test "should create user" do
    assert_difference 'User.count' do
      post register_path, params: { user: {
          email: "example2@mail.com",
          password: "password",
          password_confirmation: "password"
      } }
    end
    assert_response :success
    jdata = JSON.parse response.body
    assert jdata['success']
  end

  test "should get errors for sending empty user request" do
    post register_path
    assert_response :success
    jdata = JSON.parse response.body
    assert_not jdata['success']
    assert_not_empty jdata['errors']
  end

  test "should not create user for empty user request" do
    assert_no_difference 'User.count' do
      post register_path
    end
  end

  test "should get errors for sending duplicate email" do
    post register_path, params: { user: {
        email: @user.email,
        password: "password",
        password_confirmation: "password"
    } }
    jdata = JSON.parse response.body
    assert_not jdata['success']
    assert_not_empty jdata['errors']
  end

  test "should not create user for duplicate email" do
    assert_no_difference 'User.count' do
      post register_path, params: { user: {
          email: @user.email,
          password: "password",
          password_confirmation: "password"
      } }
    end
  end
end