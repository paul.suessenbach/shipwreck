require 'test_helper'

class Api::V1::BuildingPlanControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @player = users(:player)
    sign_in @player
  end

  test "should get all building plans" do
    get api_v1_building_plan_index_path
    assert_response :success
    jdata = JSON.parse response.body
    assert_not_nil jdata['building_plans']
  end

end
