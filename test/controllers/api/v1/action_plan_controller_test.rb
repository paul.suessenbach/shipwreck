require 'test_helper'

class Api::V1::ActionPlanControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @player = users(:player)
    sign_in @player
  end

  test "should get all action plans" do
    get api_v1_action_plan_index_path
    assert_response :success
    jdata = JSON.parse response.body
    assert_not_nil jdata['action_plans']
  end

end
