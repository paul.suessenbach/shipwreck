require 'test_helper'

class Api::V1::PlayerActionControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @player = users(:player)
    @status = @player.game_session.status
    @status.actions = 1
    @status.save!
    sign_in @player
  end

  test "should respond with tile for action discover" do
    tile = tiles(:unknown_tile)

    post api_v1_player_action_index_path, params: {
      player_action: {
        name: 'action.discover',
        tile_id: tile.id
      }
    }
    assert_response :success
    jdata = JSON.parse response.body
    assert_not_nil jdata['tiles']
    assert_equal tile.id, jdata['tiles'][0]['id']
  end

  test "should not allow action on wrong tile" do
    tile = tiles(:plains_tile)

    assert_raises Exception do
      post api_v1_player_action_index_path, params: {
        player_action: {
          name: 'action.gather_logs',
          tile_id: tile.id
        }
      }
    end
  end

  test "should add actions for end day action" do
    assert_difference '@status.actions', 3 do
      post api_v1_player_action_index_path, params: {
        player_action: {
          name: 'action.end_day',
        }
      }
    end
  end

end
