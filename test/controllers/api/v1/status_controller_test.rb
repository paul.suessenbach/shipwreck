require 'test_helper'

class Api::V1::StatusControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @player= users(:player)
    sign_in @player
  end

  test "should return user resources" do
    status = statuses(:player_status)
    assert_not_nil status

    get api_v1_status_path(status)
    assert_response :success
  end
end
