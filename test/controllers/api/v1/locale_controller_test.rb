require 'test_helper'

class Api::V1::LocaleControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @user = users(:one)
    sign_in @user
  end

  test "should get English locale" do
    get api_v1_locale_index_path, headers: { 'HTTP_ACCEPT_LANGUAGE' => 'en'}
    assert_response :success
    jdata = JSON.parse response.body
    assert_equal "en", jdata['locale']
  end

  test "should get German locale" do
    get api_v1_locale_index_path, headers: { 'HTTP_ACCEPT_LANGUAGE' => 'de'}
    assert_response :success
    jdata = JSON.parse response.body
    assert_equal "de", jdata['locale']
  end

  test "should not get French locale" do
    get api_v1_locale_index_path, headers: { 'HTTP_ACCEPT_LANGUAGE' => 'fr'}
    assert_response :success
    jdata = JSON.parse response.body
    assert_nil jdata['locale']
  end
end
