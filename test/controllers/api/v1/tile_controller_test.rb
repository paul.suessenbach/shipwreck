require 'test_helper'

class Api::V1::TileControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @no_session_user = users(:two)
    @player = users(:player)
    sign_in @player
  end

  test "should get all tiles" do
    get api_v1_tile_index_path
    assert_response :success
    jdata = JSON.parse response.body
    assert_not_nil jdata['tiles']
  end

  test "should return status not_found if no tiles available" do
    sign_in @no_session_user
    get api_v1_tile_index_path(@user)
    assert_response :no_content
  end

  test "should include buildings in tiles response" do
    get api_v1_tile_index_path
    jdata = JSON.parse response.body
    assert_match 'building.town_hall', response.body
  end
end
