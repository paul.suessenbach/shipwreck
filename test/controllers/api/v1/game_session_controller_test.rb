require 'test_helper'

class GameSessionControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @user = users(:one)
    @other_user = users(:two)
  end

  test "should not create session when not logged in" do
    assert_no_difference 'GameSession.count' do
      post api_v1_game_session_index_path
    end
    assert_response :forbidden
  end

  test "should create session when logged in" do
    sign_in @user
    assert_difference 'GameSession.count', 1 do
      post api_v1_game_session_index_path
    end
    assert_response :success
    jdata = JSON.parse response.body
    assert_equal @user.id, jdata['game_session']['user_id']
  end

  test "should return game session if available" do
    sign_in @user
    GameSession.create!({ user_id: @user.id })
    game_session = GameSession.where(user_id:  @user.id).first
    game_session.create_status!
    get api_v1_game_session_index_path(@user)
    assert_response :success
    jdata = JSON.parse response.body
    assert_equal @user.id, jdata['game_session']['user_id']
  end

  test "should return status not_found if no game_session available" do
    sign_in @user
    get api_v1_game_session_index_path(@user)
    assert_response :no_content
  end

  test "should create new island with new session" do
    sign_in @user
    assert_difference 'Tile.count', 16 do
      post api_v1_game_session_index_path
    end
    tiles = @user.game_session.tiles
    assert_equal 16, tiles.length
  end

  test "should create town hall with new session" do
    sign_in @user
    assert_difference 'Building.count', 1 do
      post api_v1_game_session_index_path
    end
  end

  test "should destory game session for current user" do
    sign_in @user
    GameSession.create!({ user_id: @user.id })
    assert_difference 'GameSession.count', -1 do
      delete api_v1_game_session_path(@user.game_session)
    end
    assert_nil @user.reload.game_session
  end

  test "should not destory game session for wrong user" do
    sign_in @user
    GameSession.create!({ user_id: @user.id })
    GameSession.create!({ user_id: @other_user.id })
    assert_no_difference 'GameSession.count' do
      assert_raises Exception do
        delete api_v1_game_session_path(@other_user.game_session)
      end
    end
    assert_not_nil @other_user.reload.game_session
  end

end
