require 'test_helper'

class Api::V1::BuildingControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @player = users(:player)
    @building_plan = building_plans(:mason)

    sign_in @player
    post api_v1_game_session_index_path
    @tile = Tile.where(game_session_id: @player.game_session.id).first
  end

  test "should create building" do
    assert_difference 'Building.count', 1 do
      post api_v1_building_index_path, params: { building: {
          name: @building_plan.name,
          tile_id: @tile.id
      } }
    end
  end

  test "should consume resources when creating building" do
    assert_difference '@player.game_session.status.reload.lumber', -@building_plan.costs_lumber do
      post api_v1_building_index_path, params: { building: {
          name: @building_plan.name,
          tile_id: @tile.id
      } }
    end
  end

  test "should raise exception if not enough resources for building creation" do
    # Set lumber to zero (required for building: 2)
    @player.game_session.status.lumber = 0
    @player.game_session.status.save!

    assert_no_difference 'Building.count' do
      assert_raises Exception do
        post api_v1_building_index_path, params: { building: {
          name: @building_plan.name,
          tile_id: @tile.id
        } }
      end
    end
  end
end
