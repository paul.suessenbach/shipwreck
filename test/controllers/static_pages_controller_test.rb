require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get root_url
    assert_response :success
  end

  test "should redirect play to home if not signed in" do
    get play_url
    assert_redirected_to root_path
  end

end
