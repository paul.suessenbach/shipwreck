require 'test_helper'

class PlayerActionTest < ActiveSupport::TestCase
  def setup
    @plains_tile = tiles(:plains_tile)
    @forest_tile = tiles(:forest_tile)
    @unknown_tile = tiles(:unknown_tile)
    @status = statuses(:player_status)
  end

  test "should be valid" do
    player_action = PlayerAction.new(name: "action.gather_food", tile_id: @plains_tile.id)
    assert player_action.valid?
  end

  test "should be invalid without a name" do
    player_action = PlayerAction.new
    assert player_action.invalid?
  end

  test "should be invalid without a plan" do
    player_action = PlayerAction.new(name: "action.get_creative")
    assert player_action.invalid?
  end

  test "should gather food" do
    player_action = PlayerAction.new(name: "action.gather_food", tile_id: @plains_tile.id)
    food_before = @status.food
    assert_difference '@status.actions', -1 do
      player_action.gather_food(@status)
    end
    assert @status.reload.food > food_before
  end

  test "should gather logs" do
    player_action = PlayerAction.new(name: "action.gather_logs", tile_id: @forest_tile.id)
    logs_before = @status.logs
    assert_difference '@status.actions', -1 do
      player_action.gather_logs(@status)
    end
    assert @status.reload.logs > logs_before
  end

  test "should discover unknown tile" do
    player_action = PlayerAction.new(name: "action.discover", tile_id: @unknown_tile.id)
    assert_not @unknown_tile.visible?
    assert_difference '@status.actions', -1 do
      player_action.discover(@status)
    end
    assert @unknown_tile.reload.visible?
  end

  test "should fail to discover already visible tile" do
    player_action = PlayerAction.new(name: "action.discover", tile_id: @forest_tile.id)
    assert @forest_tile.visible
    assert_raise Exception do
      player_action.discover(@status)
    end
  end
end
