require 'test_helper'

class BuildingPlanTest < ActiveSupport::TestCase
  test "should be valid" do
    building_plan = BuildingPlan.new(name: "buildings.lumberjack", costs_lumber: 2)
    assert building_plan.valid?
  end
end
