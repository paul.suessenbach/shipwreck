require 'test_helper'

class TileTest < ActiveSupport::TestCase
  def setup
    @player = users(:player)
  end

  test "should include terrain in json if tile is visible" do
    tile = @player.game_session.tiles.build(visible: true, terrain: 'forest')
    json = tile.to_json
    jdata = JSON.parse json
    assert_equal 'forest', jdata['terrain']
  end

  test "should not include terrain in json if tile is not visible" do
    tile = @player.game_session.tiles.build(visible: false, terrain: 'forest')
    json = tile.to_json
    jdata = JSON.parse json
    assert_nil jdata['terrain']
  end

  test "should not include created_at in json" do
    tile = @player.game_session.tiles.build(visible: true, terrain: 'forest')
    json = tile.to_json
    jdata = JSON.parse json
    assert_nil jdata['created_at']
  end
end
