require 'test_helper'

class GameSessionTest < ActiveSupport::TestCase
  def setup
    @user = users(:one)
  end

  test "should be valid" do
    game_session = GameSession.new(user_id: @user.id)
    assert game_session.valid?
  end

  test "should not be valid without user id" do
    game_session = GameSession.new(user_id: nil)
    assert_not game_session.valid?
  end
end
