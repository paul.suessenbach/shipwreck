require 'test_helper'

class StatusTest < ActiveSupport::TestCase
  def setup
    @player_status = statuses(:player_status)
  end

  test "should not include created_at in json" do
    json = @player_status.to_json
    jdata = JSON.parse json
    assert_nil jdata['created_at']
  end

  test "should have food available" do
     assert @player_status.available?("food")
  end

  test "should have nil available" do
    assert @player_status.available?(nil)
  end

  test "should not have water available" do
    assert_not @player_status.available?("water")
  end

  test "should notify about victory when producing a ship" do
    @player_status.produce "ship"
    assert @player_status.victory?
    assert_not_nil @player_status.notification
  end
end
