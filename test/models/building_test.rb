require 'test_helper'

class BuildingTest < ActiveSupport::TestCase
  def setup
    @tile = tiles(:forest)
  end

  test "should be valid" do
    building = @tile.build_building(name: "buildings.mason", symbol: "building")
    assert building.valid?
  end

  test "should be invalid if on tile already taken" do
    building = @tile.build_building(name: "buildings.mason", symbol: "building")
    building.save!
    assert building.invalid?
  end

  test "should be invalid if not town hall and no building plan present" do
    building = @tile.build_building(name: "buildings.lumberjack", symbol: "building")
    assert building.invalid?
  end

  test "should be valid if if town hall" do
    building = @tile.build_building(name: "buildings.town_hall", symbol: "building")
    assert building.valid?
  end
end
