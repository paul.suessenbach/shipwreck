# Shipwreck

Die folgenden Seiten könnten auch von Interesse sein:

* [Englische README](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/README-de.md)
* [Deployment auf Heroku](https://damp-meadow-55369.herokuapp.com/)

Shipwreck ist ein einfaches Überlebensspiel: Gestranded auf einer kleiner Insel musst du
ein Schiff bauen, um zu entkommen.

Es ist außerdem ein Portfolio-Projekt, was auch die nicht ganz so spannende Natur des Spiels
erklärt. Diese README konzentriert sich mehr auf diesen
Aspekt des Projekts. Falls du an dem Spiel selbst interessiert bist, dann solltest du die
[Anleitung](https://damp-meadow-55369.herokuapp.com/manual) lesen.

# Stuktur

Shipwreck ist eine eher einfache Rails Anwendung. Sie hat ein paar statische Seiten,
ein grundlegendes Authetifikationssystem mit Benutzerverwaltung und - als Hauptbestandteil -
eine Rest API, die mit einem JavaScript Frontend kommuniziert.

Diese REST API ermöglicht ein einfaches Spiel: Der Benutzer kann auf einer Insel Akionen
ausführen und Gebäude errichten.  

Das Hauptobjekt auf der Model-Seite ist die
[GameSession](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/models/game_session.rb),
welche als Contrainer für alle spiel-bezogenen Daten agiert. Obwohl User und GameSession in
einer 1:1 Beziehung stehen, ist GameSession ein eigenständiges Model - einerseits, um Daten
klar zu trennen, anderseits, um GameSessions sauber zu zerstören und dabei mit Hilfe von
``dependend: destroy``auch die von der GameSession abhängigen Records zu entsorgen.
Das Erzeugen eines GameSession Records erstellt einen kompletten Satz
[Tile](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/models/tile.rb)-Records,
welche die Insel repräsentieren.

Benutzer-Interaktion erfolgt nach Spielstart über POST-Requests an den _Buildings-_ oder den
_PlayerActionController_. Nach dem Prinzip "traue niemals dem Frontend" werden alle Requests
ausführlich validiert. Da sowohl das PlayerAction- als auch das Building-Model Felder haben,
die in Kombination mit anderen Felder gültig oder ungültig sind, haben beide Klassen spezielle
Validatoren (zu finden natürlich in
[app/validators](https://gitlab.com/paul.suessenbach/shipwreck/tree/master/app/validators)).  

# Authentication

Authentication und User-Management sind aufwendige, fehleranfällige, aber im Prinzip gelöste
Aufgaben. Ich habe deshalb auf selbstgestrickte Lösungen verzichtet und die Aufgabe über das
[Devise](https://github.com/plataformatec/devise) Gem geregelt.
Die einzig große Veränderung ist ein Hauch von mehr "Web 2.0", indem An-/Abmeldungen und
Registrierung über Dialoge auf jeder beliebigen Seite ausgeführt werden, anstatt eine extra
Seite zu haben.

Es gibt ein einziges Model
[(User)](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/models/user.rb),
welches komplett auf von Devise generierten Code basiert.
Auf der Controller-Seite sind sowohl
[SessionsController](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/controllers/users/sessions_controller.rb)
als auch
[RegistrationsController](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/controllers/users/registrations_controller.rb)
maßgeschneiderte Controller, die von den jeweiligen Devise-Klassen erben und Anfragen über REST
regeln. Sprich: Die Controller erwartet und senden JSON. Rails/Devise
Request-From-Forgery-Mechanismus erwartet eigentlich, dass nach einer erfolgreichen Anmeldung
ein Seite Reload erfolgt. Da dies hier nicht der Fall ist, werden die entsprechenden
Informationen in der JSON Response mitgeliefert.

# Internationalization

Das gesamte Projekt mit der Hilfe von Rail I18n Gem internationalisiert, sowohl auf der
Backend als auch auf der Frontend Seite. Als Referenz für die vom Benutzer gewünschte
Sprache wird dabei der HTTP_ACCEPT_LANGUAGE Header herangezogen. Parsen dieses Headers
geschieht mit Hilfe des [http_accept_language](https://github.com/iain/http_accept_language)
Gems.

Javascript hat keine schöne Möglichkeit, die vom Benutzer gewünschte Sprache festzustellen.
Um die Information auch auf der Frontend-Seite verfügbar (und einheitlich) zu haben, verfügt
das Projekt über einen
[Locale Controller](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/controllers/api/v1/locale_controller.rb),
der den HTTP-Header parst und die Information an das Frontend zurückliefert.

Um sicherzustellen, dass alle Keys in allen locale-Dateien vorhanden sind, verfügt das Projekt
über einen
[entsprechenden Test](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/test/i18n_test.rb).

# Frontend

Das Frontend soll ein wirkliches JavaScript Frontend einer echten Anwendung simulieren.
Das Template der _Play_-Seite besteht nur aus einem einzigen div (_#board_). Die
JavaScript-Anwendung übernimmt von dort und erstellt das gesamte Spiel. Alle Kommunikation
mit dem Back-end erfolgt über REST Calls.

Das Frontend benutzt JQuery und Bootstrap, aber kein fortgeschrittenes Framework wie Ember
oder AngularJS. Stattdessen setzt es auf einige einfache Controller, Views und macht Gebrauch
von Handlebars-Templates.

# Tests

Auch wenn es nur eine eher einfache Rails-Anwendung ist, hat das Projekt selbstverständlich
Tests. Da die Logik des Projekts sich auf die Models konzentriert, sind hauptsächlich Unit
Tests für die Models vorhanden. Es gibt allerdings auch ein paar Controllertests. Nicht
vorhanden sind Tests für die gesamte Javascript Frontend-Anwendung - dies ist keine
Demonstration meiner Javascript-Skills (Spoiler: nicht so gut).