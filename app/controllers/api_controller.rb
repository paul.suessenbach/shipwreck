class ApiController < ApplicationController
  protect_from_forgery
  before_action :check_user_login

  private

    # Check if a logged in user exists, respond with 403 Forbidden otherwise
    def check_user_login
      head :forbidden unless user_signed_in?
    end
end