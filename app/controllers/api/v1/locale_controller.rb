class Api::V1::LocaleController < ApiController
  def index
    locale = http_accept_language.compatible_language_from(I18n.available_locales)
    render json: { locale: locale }
  end
end
