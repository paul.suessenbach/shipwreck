class Api::V1::BuildingController < ApiController
  def create
    status = current_user.game_session.status
    building = Building.create!(building_params, status)
    render json: {
      building: building,
      status: status
    }
  end

  def show
  end

  def destroy
  end

  def building_params
    params.require(:building).permit(:name, :tile_id)
  end

end
