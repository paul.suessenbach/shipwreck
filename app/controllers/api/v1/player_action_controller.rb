class Api::V1::PlayerActionController < ApiController
  def create
    player_action = PlayerAction.new(player_action_params)
    game_session = current_user.game_session
    status = game_session.status

    if player_action.name == 'action.end_day'
      return handle_end_day(player_action, status)
    end

    raise Exception.new('Action not allowed') if player_action.invalid?
    raise Exception.new("No more actions left today") unless status.actions_left?
    tile = Tile.where("id = ?", player_action.tile_id).take!
    raise Exception.new("Tile does not belong to user's game session") if tile.game_session_id != game_session.id

    player_action.public_send player_action.action, status
    render json: player_action.as_json_response(status, tile)
  end

  def player_action_params
    params.require(:player_action).permit(:tile_id, :name)
  end

  # Handle the special action of ending a day
  def handle_end_day(player_action, status)
    player_action.end_day(status)
    render json: {
      player_action: player_action,
      status: status,
      notification: status.notification
    }
  end
end
