class Api::V1::StatusController < ApiController

  def index
    render json: {
      status: current_user.game_session.status
    }
  end

def show
    unless current_user.game_session.status.id.to_s == params[:id]
      raise Exception.new("Resource id does not belong to user's game session")
    end
    render json: current_user.game_session.status
  end
end
