class Api::V1::GameSessionController < ApiController
  # Get the game session for the current user, if any
  def index
    @game_session = GameSession.where(user_id: current_user.id).first
    return render json: {}, status: :no_content if @game_session.nil?
    render json: {
        game_session: @game_session
    }
  end

  # Start a new free-play game for the current user
  def create
    @game_session = GameSession.create_for_user(current_user)
    render json: {
        game_session: @game_session
    }
  end

  # Destroy the game session for the current user
  def destroy
    user = current_user.reload
    raise Exception.new("Game session does not belong to current user") unless user.game_session.id.to_s == params[:id]
    user.game_session.destroy!
  end

end
