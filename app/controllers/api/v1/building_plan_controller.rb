class Api::V1::BuildingPlanController < ApiController
  def index
    render json: {
        building_plans: BuildingPlan.all
    }
  end
end
