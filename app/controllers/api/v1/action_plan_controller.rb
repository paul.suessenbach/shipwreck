class Api::V1::ActionPlanController < ApiController
  def index
    render json: {
        action_plans: ActionPlan.all
    }
  end
end
