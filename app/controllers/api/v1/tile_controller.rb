class Api::V1::TileController < ApiController
  def index
    game_session = current_user.game_session
    return render json: {}, status: :no_content if game_session.nil?
    render json: {
      tiles: game_session.tiles.as_json(include: :building)
    }
  end
end
