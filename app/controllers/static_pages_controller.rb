class StaticPagesController < ApplicationController
  def home
  end

  def about
  end

  def manual
    @building_plans = BuildingPlan.all
    @action_plans = ActionPlan.all
  end

  def play
    redirect_to root_path unless user_signed_in?
  end
end
