class ApplicationController < ActionController::Base
  include HttpAcceptLanguage::AutoLocale

  def after_sign_in_path_for(user)
    root_url
  end
end
