class Users::RegistrationsController < Devise::RegistrationsController
  def create
    build_resource(sign_up_params)

    if resource.save
      sign_up(resource_name, resource)
      json_success_response
    else
      render :json => { success: false, errors: resource.errors.as_json }
    end
  end

  def json_success_response
    render json: { success: true,
                   csrfParam: request_forgery_protection_token,
                   csrfToken: form_authenticity_token
    }
  end
end
