function Loader (game_session) {
    this._data = {
        game_session: game_session
    };
    this._queue = [];
    this._waiting_for = 0;
}

Loader.prototype = {
    queue_for_load: function(resource) {
        this._queue.push(resource);
        this._waiting_for++;
    },
    load: function(onSuccess) {
        var self = this;
        for (var i = 0; i < this._queue.length; i++) {
            var resource = this._queue[i];
            $.ajax({
                url: "/api/v1/" + resource,
                method: "GET"
            }).success(function (data) {
                for (var key in data) {
                    self._data[key] = data[key];
                }
                self._waiting_for--;
                if (self._waiting_for === 0) {
                    onSuccess(self._data);
                }
            });
        }
    }
};