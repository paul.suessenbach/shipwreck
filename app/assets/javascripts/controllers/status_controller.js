function StatusController() {
    var self = this;
    $('#board').on('update-session', function () {
        self.update_session();
    });
}

StatusController.prototype = {
    is_responsible: function(data) {
        if (data.controller_name == 'status' || data.controller_name == 'statuses') {
            return true;
        }
        return false;
    },
    update_session: function () {
        $('#board').trigger('status-reload');
    }
};