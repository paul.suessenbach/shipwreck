function ActionPlanController() {
    var self = this;
    var board = $('#board');
    board.off('action-trigger');
    board.on('action-trigger', function (element, data) {
        self.trigger_action(data);
    });
}

ActionPlanController.prototype = {
    is_responsible: function(data) {
        if (data.controller_name == 'action-plans' || data.controller_name == 'action-plan') {
            return true;
        }
        return false;
    },
    trigger_action: function (data) {
        var tile_id = $('#tile_menu').data('source-tile-id');
        var post_data = {
            player_action: {
                name: data.model.name,
                tile_id: tile_id
            }
        };
        var self = this;
        $.ajax({
            url: "/api/v1/player_action/",
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(post_data)
        }).success(function (data) {
            self.handle_response(data);

            $('.active').each(function () {
                $(this).removeClass('active');
            });
            $('#tile_menu').addClass('hidden');
            $('#building_menu').addClass('hidden');
            var board = $('#board');
            board.trigger('update-view', {
                model_name: 'tile',
                model: data
            });
        });
    },
    end_day: function() {
        var self = this;
        $.ajax({
            url: "/api/v1/player_action/",
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                player_action: {
                    name: 'action.end_day'
                }
            })
        }).success(function (data) {
            self.handle_response(data);
        });
    },
    handle_response: function (data) {
        for (var key in data) {
            var value = data[key];
            if (Array.isArray(value)) {
                for (var i = 0; i < value.length; i++) {
                    var model = value[i];
                    $('#board').trigger('update-view', {
                        model_name: key,
                        model: model
                    });
                }
            } else {
                $('#board').trigger('update-view', {
                    model_name: key,
                    model: value
                });
            }
        }
    }
};