function BuildingPlanController() {
    var self = this;

    $('#tile_menu').on('building-creation', function(element, data) {
        self.create_building(data);
    });
}

BuildingPlanController.prototype = {
    is_responsible: function(data) {
        if (data.controller_name == 'building-plans' || data.controller_name == 'building-plan') {
            return true;
        }
        return false;
    },
    create_building: function(data) {
        var tile_id = $('#tile_menu').data('source-tile-id');
        var post_data = {
            building: {
                name: data.model.name,
                tile_id: tile_id
            }
        };
        var self = this;
        $.ajax({
            url: "/api/v1/building/",
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(post_data)
        }).success(function(data) {
            self.handle_response(data);

            $('.active').each(function() {
                $(this).removeClass('active');
            });
            $('#tile_menu').addClass('hidden');
        });
    },
    handle_response: function (data) {
        for (var key in data) {
            var value = data[key];
            if (Array.isArray(value)) {
                for (var i = 0; i < value.length; i++) {
                    var model = value[i];
                    $('#board').trigger('update-view', {
                        model_name: key,
                        model: model
                    });
                }
            } else {
                $('#board').trigger('update-view', {
                    model_name: key,
                    model: value
                });
            }
        }
    }
};