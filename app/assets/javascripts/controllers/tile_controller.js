function TileController(parent) {
    var self = this;

    parent.off('tile-clicked');
    parent.on('tile-clicked', function(element, data) {
        self.click_tile(data);
    });
    parent.off('building-clicked');
    parent.on('building-clicked', function(element, data) {
        self.click_building(data);
    });
}

TileController.prototype = {
    is_responsible: function(data) {
        if (data.controller_name == 'tile' || data.controller_name == 'tiles') {
            return true;
        }
        return false;
    },
    click_tile: function(data) {
        var jquery_element = $(data.source);
        var tile_menu = $('#tile_menu');
        $('#building_menu').addClass('hidden');
        if (jquery_element.hasClass('active')) {
            jquery_element.removeClass('active');
            tile_menu.addClass('hidden');
        } else {
            var board = $('#board');
            board.trigger('update-view', {
                model_name: 'building-plans',
                model: data.model
            });
            board.trigger('update-view', {
                model_name: 'action-plans',
                model: data.model
            });
            if (this.last_active_element !== undefined) {
                this.last_active_element.removeClass('active');
            }
            jquery_element.addClass('active');
            this.last_active_element = jquery_element;
            var position = jquery_element.offset();
            position.left += 50;
            tile_menu.css(position);
            if (data.model.row > 2) {
                tile_menu.addClass('menu-up');
            } else {
                tile_menu.removeClass('menu-up');
            }
            tile_menu.data('source-tile-id', data.model.id);
            tile_menu.removeClass('hidden');
        }
    },
    click_building: function(data) {
        var jquery_element = $(data.source);
        var building_menu = $('#building_menu');
        var tile_menu = $('#tile_menu');
        tile_menu.addClass('hidden');
        if (jquery_element.hasClass('active')) {
            jquery_element.removeClass('active');
            building_menu.addClass('hidden');
        } else {
            var board = $('#board');
            board.trigger('update-view', {
                model_name: 'building-action-item',
                model: data.model
            });
            board.trigger('update-view', {
                model_name: 'building-action',
                model: data.model
            });
            if (this.last_active_element !== undefined) {
                this.last_active_element.removeClass('active');
            }
            jquery_element.addClass('active');
            this.last_active_element = jquery_element;
            var position = jquery_element.offset();
            position.left += 50;
            building_menu.css(position);
            if (data.model.row > 2) {
                building_menu.addClass('menu-up');
            } else {
                building_menu.removeClass('menu-up');
            }
            // Set data to tile-menu instead of building-menu, because action-plan controller expects it there
            tile_menu.data('source-tile-id', data.model.id);
            building_menu.removeClass('hidden');
        }
    }
};