$(document).ready(function () {
    if (!Array.isArray) {
        Array.isArray = function(arg) {
            return Object.prototype.toString.call(arg) === '[object Array]';
        };
    }

    Handlebars.registerHelper('i18n',
        function(str, options){
            var hash = options.hash;
            return I18n.t(str, { count: hash.count, name: hash.name , param: hash.param });
            // return (I18n != undefined ? I18n.t(str) : str);
        }
    );
    Handlebars.partials = HandlebarsTemplates;

    Game.initialize();
    Game.show_overlay();
    $.ajax({
        url: "api/v1/locale",
        method: "GET"
    }).success(function(data) {
        Game.set_locale(data.locale);
        Game.render_session();
    });
});