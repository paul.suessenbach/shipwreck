# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $.ajaxSetup({
    dataType: 'json'
  })

  $("form#sign_in_user").on "ajax:success", (event) ->
    data = event.detail[0]
    if data.success
      $('#login_form').modal('hide')
      login data
    else
      errorContainer = $('form#sign_in_user div.container')
      errorContainer.empty()
      for error in data.errors
        errorContainer.append("<div class='alert alert-danger'>" + error + "</div>")
      errorContainer.removeClass('hidden')
  $("form#user_logout").on "ajax:success", (event) ->
    data = event.detail[0]
    if data.success
      $('meta[name="csrf-token"]').attr('content', data.csrfToken)
      $("input[name*='#{ data.csrfParam }'").each ->
        $(this).attr('content', data.csrfToken)
      logout data
  $("form#sign_up_user").on "ajax:success", (event) ->
    data = event.detail[0]
    if data.success
      $('#register_form').modal('hide');
      login data
    else
      $("form#sign_up_user").render_form_errors('user', data.errors)

  $.fn.render_form_errors = (model_name, errors) ->
    form = this
    this.clear_form_errors()
    $.each(errors, (field, messages) ->
      input = form.find('input, select, textarea').filter(->
        name = $(this).attr('name')
        if name
          name.match(new RegExp(model_name + '\\[' + field + '\\(?'))
      )
      input.addClass('is-invalid')
      input.parent().append('<span class="invalid-feedback">' + $.map(messages, (m) -> m.charAt(0).toUpperCase() + m.slice(1)).join('<br />') + '</span>')
    )

  $.fn.clear_form_errors = () ->
    this.find('.is-invalid').each ->
      $(this).removeClass('is-invalid')
    this.find('span.invalid-feedback').remove()

  login = (data) ->
    $('meta[name="csrf-token"]').attr('content', data.csrfToken)
    $("input[name*='#{ data.csrfParam }'").each ->
      $(this).attr('content', data.csrfToken)
    $('.user-logged-out').each ->
      $(this).addClass('hidden')
    $('.user-link').each ->
      $(this).removeClass('disabled')
    $('.user-logged-in').each ->
      $(this).removeClass('hidden')

  logout = (data) ->
    $('.user-logged-out').each ->
      $(this).removeClass('hidden')
    $('.user-link').each ->
      $(this).addClass('disabled')
    $('.user-logged-in').each ->
      $(this).addClass('hidden')