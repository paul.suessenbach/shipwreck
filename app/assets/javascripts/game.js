var Game = Game || {
    initialize: function () {
        var self = this;
        var board = $('#board');
        board.on('click', 'button', function () {
            var data = $(this).data();
            if (data.id !== undefined && data.method !== undefined) {
                self[data.method](data.id);
            }
            if (data.controller !== undefined && data.command !== undefined) {
                $('#board').trigger('controller-command', {
                    controller_name: data.controller,
                    command: data.command
                });
            }
        });

        var overlay = HandlebarsTemplates['overlay']();
        $('body').append(overlay);
    },
    show_overlay: function() {
        $('.overlay').removeClass('hidden');
    },
    hide_overlay: function() {
        $('.overlay').addClass('hidden');
    },
    set_locale: function(locale) {
        if (locale !== undefined) {
            I18n.locale = locale;
        }
    },
    render_session: function () {
        var self = this;
        $.ajax({
            url: "/api/v1/game_session",
            method: "GET"
        }).success(function (data, txt, xhr) {
            if (xhr.status === 200) {
                self.load_all_data(data);
            } else {
                self.render_start_session();
            }
        }).error(function () {
            self.render_start_session();
        })
    },
    render_start_session: function () {
        var html = HandlebarsTemplates['start_session']();
        var board = $('#board');
        board.empty();
        board.removeClass('active-game');
        board.append(html);
        this.hide_overlay();
    },
    do_start_session:function(ignored) {
        this.show_overlay();
        var self = this;
        $.ajax({
            url: "/api/v1/game_session",
            method: "POST"
        }).success(function (data) {
            self.load_all_data(data)
        });
    },
    load_all_data: function(data) {
        var self = this;
        var loader = new Loader(data.game_session);
        loader.queue_for_load('tile');
        loader.queue_for_load('action_plan');
        loader.queue_for_load('building_plan');
        loader.queue_for_load('status');
        loader.load(function(computed_data) {
            self.render_board(computed_data);
        });
    },
    render_board: function (computed_data) {
        // Store length of session
        window.sessionStorage.setItem('game_session.days_available', computed_data.game_session.days_available);

        this.views = [];
        this.controllers = [];
        var board = $('#board');
        board.empty();
        board.addClass('active-game');
        var self = this;

        // Register resouces reload event
        board.on('status-reload', function() {
            $.ajax({
                url: "/api/v1/status/" + self.resource_id,
                method: "GET"
            }).success(function (data) {
                $('#board').trigger('update-view', {
                    model_name: "statuses",
                    model: data
                });
            })
        });

        // Create tile menu
        var tile_menu_view = new TileMenuView();
        tile_menu_view.create(board);
        this.views.push(tile_menu_view);
        var building_menu_view = new BuildingMenuView();
        building_menu_view.create(board);
        this.views.push(building_menu_view);

        var tile_menu_actions = $('#buildings_plans');
        var building_menu_actions = $('#building_actions');

        for (var i = 0; i < computed_data.action_plans.length; i++) {
            var action_plan = computed_data.action_plans[i];
            if (action_plan.require_building) {
                var view = new BuildingActionItemView(action_plan);
                view.create(building_menu_actions);
                this.views.push(view);
            } else {
                var view = new ActionPlanView(action_plan);
                view.create(tile_menu_actions);
                this.views.push(view);
            }
        }
        for (var i = 0; i < computed_data.building_plans.length; i++) {
            var building_plan = computed_data.building_plans[i];
            var view = new BuildingPlanView(building_plan);
            view.create(tile_menu_actions);
            this.views.push(view);
        }
        this.controllers.push(new ActionPlanController());
        this.controllers.push(new BuildingPlanController());

        // Create notification view
        var notification_view = new NotificationView();
        notification_view.create(board);
        this.views.push(notification_view);

        // Create tiles
        var row = $('<div class="row justify-content-md-center">');
        board.append(row);
        var lastRow = 1;
        for (var i = 0; i < computed_data.tiles.length; i++) {
            var tile = computed_data.tiles[i];
            if (tile.row > lastRow) {
                lastRow = tile.row;
                row = $('<div class="row justify-content-md-center">');
                board.append(row);
            }
            var view = new TileView(tile);
            view.create(row);
            this.views.push(view);
        }
        this.controllers.push(new TileController(board));

        // Create status bars
        var status_view = new StatusView(computed_data.status);
        status_view.create(board);
        this.resource_id = computed_data.status.id;
        this.views.push(status_view);
        this.controllers.push(new StatusController());

        // Register view update events
        board.off('update-view');
        board.on('update-view', function(event, data) {
            for (var i = 0; i < self.views.length; i++) {
                var view = self.views[i];
                if (view.is_responsible(data)) {
                    view.update(data);
                }
            }
        });

        // Register controller update events
        board.off('controller-command');
        board.on('controller-command', function(event, data) {
            for (var i = 0; i < self.controllers.length; i++) {
                var controller = self.controllers[i];
                if (controller.is_responsible(data)) {
                    controller[data.command]();
                }
            }
        });

        // Hide loading overlay
        this.hide_overlay();
    },
    abort_session: function(game_session_id) {
        var self = this;
        if (window.confirm(I18n.t('play.are_you_sure'))) {
            self.show_overlay();
            $.ajax({
                url: "api/v1/game_session/" + game_session_id,
                method: "DELETE"
            }).success(function(data) {
                self.render_start_session();
            });
        }
    }
};