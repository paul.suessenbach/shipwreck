function StatusView(model) {
    this._model = model;
}

StatusView.prototype = {
    is_responsible: function(data) {
        if (data.model_name === 'statuses' || data.model_name === 'status') {
            if (data.model.id === this._model.id) {
                return true;
            }
        }
        return false;
    },
    create: function (parent) {
        var self = this;
        var html_resource_bar = HandlebarsTemplates['resource_bar'](this._convert_model(this._model));
        parent.append(html_resource_bar);
        this._resource_bar = parent.find(".resource-bar");
        var html_status_bar = HandlebarsTemplates['status_bar'](this._model);
        parent.prepend(html_status_bar);
        this._status_bar = parent.find(".status-bar");

        this._store_resources_in_session();
        this._set_day_status();
    },
    update: function(data) {
        this._model = data.model;
        this._store_resources_in_session();
        this._set_day_status();

        for (var key in data.model) {
            if (key !== 'id') {
                var property_element = $(this._resource_bar).find("[data-property='" + key + "']");
                if (property_element.length === 0) {
                    property_element = $(this._status_bar).find("[data-property='" + key + "']");
                }
                property_element.html(data.model[key]);
            }
        }
    },
    _convert_model: function (model) {
        var converted_model = {
            id: model.id,
            resources: []
        };
        var blacklist = [
            'id',
            'actions',
            'max_actions',
            'game_session_id',
            'days_left'
        ];
        for (var key in model) {
            if (blacklist.indexOf(key) === -1) {
                var resource_name = key;
                var resource_value = model[key];
                converted_model.resources.push({
                    name: resource_name,
                    value: resource_value
                });
            }
        }
        return converted_model;
    },
    _store_resources_in_session: function () {
        window.sessionStorage.setItem('resource.stones', this._model.stones);
        window.sessionStorage.setItem('resource.lumber', this._model.lumber);
        window.sessionStorage.setItem('resource.actions', this._model.actions);
    },
    _set_day_status: function() {
        var enable_day_end = this._model.actions > 0 || this._model.max_actions == 0;
        this._status_bar.find("[data-command='end_day']").prop('disabled', enable_day_end);

        var width = this._model.days_left * 1.0 / window.sessionStorage.getItem('game_session.days_available') * 100.0;
        var status_bar = this._status_bar.find("[data-property='days_left']");
        if (width <= 20) {
            status_bar.removeClass('bg-warning');
            status_bar.addClass('bg-danger');
        } else if (width <= 50) {
            status_bar.addClass('bg-warning');
            status_bar.removeClass('bg-success');
        }
        status_bar.width(width + "%");
        status_bar.html(width / 10);
    }
};