function ActionPlanView(model) {
    this._model = model;
}

ActionPlanView.prototype = {
    is_responsible: function(data) {
        if (data.model_name == 'action-plans') {
            return true;
        }
        return false;
    },
    create: function(parent) {
        var self = this;
        var html = HandlebarsTemplates['action_plan'](this._model);
        parent.append(html);
        this._element = parent.find("[data-id='" + this._model.id + "']");

        $(parent).on('click', 'button', function() {
            var element = $(this);
            if (element.data('model') === 'action-plan' && element.data('id') === self._model.id) {
                element.trigger('action-trigger', {
                    source: element,
                    model: self._model
                });
            }
        });
    },
    update: function(data) {
        if (window.sessionStorage.getItem('resource.actions') == 0) {
            this._element.prop('disabled', true);
            return;
        }

        if (data.model.visible && this._model.require_invisible) {
            this._element.prop('disabled', true);
            return;
        }

        if (this._model.possible_terrain === 'all' || this._model.possible_terrain === data.model.terrain) {
            this._element.prop('disabled', false);
        } else {
            this._element.prop('disabled', true);
        }
    }
};