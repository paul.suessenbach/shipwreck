function NotificationView() {
}

NotificationView.prototype = {
    is_responsible: function (data) {
        if (data.model_name === 'notification' || data.model_name === 'notifications') {
            return true;
        }
        return false;
    },
    create: function (parent) {
        var html = HandlebarsTemplates['notification'];
        parent.append(html);
    },
    update: function (data) {
        var notification = $('#notification');
        if (data.model === undefined || data.model === null) {
            return;
        }
        notification.find('[data-property="title"]').html(I18n.t(data.model.title));
        notification.find('[data-property="text"]').html(I18n.t(data.model.text));
        notification.modal('show');
    }
};