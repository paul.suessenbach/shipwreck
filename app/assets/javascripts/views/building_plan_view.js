function BuildingPlanView(model) {
    this._model = model;
}

BuildingPlanView.prototype = {
    is_responsible: function(data) {
        if (data.model_name == 'building-plans') {
            return true;
        }
        return false;
    },
    create: function(parent) {
        var self = this;
        var html = HandlebarsTemplates['building_plan'](this._model);
        parent.append(html);
        this._element = parent.find("[data-id='" + this._model.id + "']");

        $(parent).on('click', 'button', function() {
            var element = $(this);
            if (element.data('model') === 'building-plan' && element.data('id') === self._model.id) {
                element.trigger('building-creation', {
                    source: element,
                    model: self._model
                });
            }
        });
    },
    update: function(data) {
        if (!data.model.visible || window.sessionStorage.getItem('resource.actions') == 0) {
            this._element.prop('disabled', true);
            return;
        }

        if (this._model.possible_terrain !== 'all' && this._model.possible_terrain !== data.model.terrain) {
            this._element.prop('disabled', true);
            return;
        }

        var stones = window.sessionStorage.getItem('resource.stones');
        var lumber = window.sessionStorage.getItem('resource.lumber');
        if (stones < this._model.costs_stones || lumber < this._model.costs_lumber) {
            this._element.prop('disabled', true);
        } else {
            this._element.prop('disabled', false);
        }
    }
};