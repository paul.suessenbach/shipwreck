function BuildingActionItemView(model) {
    this._model = model;
}

BuildingActionItemView.prototype = {
    is_responsible: function(data) {
        if (data.model_name == 'building-action-item') {
            return true;
        }
        return false;
    },
    create: function(parent) {
        var self = this;
        var html = HandlebarsTemplates['action_plan'](this._model);
        parent.append(html);
        this._element = parent.find("[data-id='" + this._model.id + "']");

        $(parent).on('click', 'button', function() {
            var element = $(this);
            if (element.data('model') === 'action-plan' && element.data('id') === self._model.id) {
                element.trigger('action-trigger', {
                    source: element,
                    model: self._model
                });
            }
        });
    },
    update: function(data) {
        if (window.sessionStorage.getItem('resource.actions') == 0) {
            this._element.prop('disabled', true);
            return;
        }

        if (data.model.building.produces === undefined || data.model.building.produces === null) {
            this._element.prop('disabled', true);
            return;
        }

        this._element.prop('disabled', false);
    }
};