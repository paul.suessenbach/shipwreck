function TileView(model) {
    this._model = model;
}

TileView.prototype = {
    is_responsible: function(data) {
        if (data.model_name === 'tiles' || data.model_name === 'tile') {
            return data.model.id === this._model.id;
        }
        if (data.model_name === 'buildings' || data.model_name === 'building') {
            return data.model.tile_id === this._model.id;
        }
        return false;
    },
    create: function(parent) {
        var self = this;
        var html = HandlebarsTemplates['tile'](this._model);
        parent.append(html);
        this._element = parent.find("[data-id='" + this._model.id + "']");
        this._element.on('click', 'i', function() {
            var image_container = $(this).parent();
            if (image_container.hasClass("building-symbol")) {
                parent.trigger('building-clicked', {
                    source: image_container,
                    model: self._model
                });
            }
            if (image_container.hasClass("empty-symbol")) {
                parent.trigger('tile-clicked', {
                    source: image_container,
                    model: self._model
                });
            }
        });
    },
    update: function(data) {
        if (data.model_name === 'buildings' || data.model_name === 'building') {
            this.update_building(data.model);
        }
        if (data.model_name === 'tiles' || data.model_name === 'tile') {
            this.update_tile(data.model);
        }
    },
    update_building: function(model) {
        this._model.building = model;

        var symbol = this._element.find("[data-property='building.symbol']");
        var name = this._element.find("[data-property='building.name']");

        if (model !== undefined) {
            symbol.removeClass('empty-symbol');
            symbol.addClass('building-symbol');
            symbol.html('<i class="fa ' + model.symbol + '"></i>');
            name.html(I18n.t(model.name));
        } else {
            symbol.addClass('empty-symbol');
            symbol.removeClass('building-symbol');
            symbol.html('<i class=fa fa-cubes></i>')
            name.html('');
        }
    },
    update_tile: function(model) {
        // Only update terrain, so that we don't overwrite the building property
        this._model.terrain = model.terrain;
        this._model.visible = model.visible;
        var terrain = this._element.addClass(model.terrain);
    }
};