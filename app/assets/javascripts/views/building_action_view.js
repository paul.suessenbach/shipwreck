function BuildingMenuView() {
}

BuildingMenuView.prototype = {
    is_responsible: function (data) {
        if (data.model_name == 'building-action') {
            return true;
        }
        return false;
    },
    create: function (parent) {
        var tile_menu = HandlebarsTemplates['building_menu']();
        parent.append(tile_menu);
        this._element = $('#building_menu');
    },
    update: function (data) {
        var building = data.model.building;
        this._element.find('[data-property="name"]').html(I18n.t(building.name));
        this._element.find('[data-property="consumes"]').html(this._build_consumes(building));
        this._element.find('[data-property="produces"]').html(this._build_produces(building));
    },
    _build_consumes: function(building) {
        if (building.consumes === undefined || building.consumes === null) {
            return "";
        }
        if (building.consumes_second === undefined || building.consumes_second === null) {
            return I18n.t(building.consumes);
        }
        return I18n.t(building.consumes) + " + " + I18n.t(building.consumes_second);
    },
    _build_produces: function (building) {
        if (building.produces === undefined || building.produces === null) {
            return "";
        }
        return I18n.t(building.produces);
    }
};