//= require handlebars.runtime
//= require i18n/translations
//= require_tree ./templates
//= require_tree ./controllers
//= require_tree ./views
//= require loader
//= require game
//= require main