# A building, occupying a field on the island
class Building < ApiModel
  belongs_to :tile
  validates_with BuildingValidator

  # Check if a building is the special case of the town hall building
  def town_hall?
    name == 'buildings.town_hall'
  end

  # Create a building from building plan
  def Building.create!(building_params, status)
    building = Building.new(building_params)
    building_plan = BuildingPlan.where(name: building.name).first
    check_and_consume_costs(building_plan, status)
    building.symbol = building_plan.symbol
    building.consumes = building_plan.consumes
    building.consumes_second = building_plan.consumes_second
    building.produces = building_plan.produces
    building.save!
    return building
  end

  # Create a town hall on a free tile
  def Building.create_town_hall!(tile)
    tile.visible = true
    tile.create_building!(name: 'buildings.town_hall', symbol: "fa-free-code-camp")
    tile.save!
  end

  private

  # Check if the player has enough resources for the building, and if so, consume them. Raise an exception otherwise.
  def Building.check_and_consume_costs(building_plan, status)
    raise Exception.new('No more actions left today') unless status.actions_left?
    if status.stones < building_plan.costs_stones || status.lumber < building_plan.costs_lumber
      raise Exception.new("Can not build #{building_plan.name}, not enough resources!")
    end

    status.stones -= building_plan.costs_stones
    status.lumber -= building_plan.costs_lumber
    status.actions -= 1
    status.save!
  end
end
