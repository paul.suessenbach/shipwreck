# Resources and pseudo-resources (like actions) for a game session
class Status < ApiModel
  # Random resources that can be found while discovering tiles
  RANDOM_RESOURCES =%w[logs stones food water]

  belongs_to :game_session

  validates :game_session_id, presence: true

  # Start session with default values
  def initialize(arguments = nil)
    super(actions: 4, lumber: 10, food: 8)
  end

  # Check if there are actions left for the day
  def actions_left?
    actions.positive?
  end

  # Check if the game session has been won
  def victory?
    ship.positive?
  end

  # Get the last notification set on this status
  def notification
    return nil if @notification.nil?
    return {
      title: "notification.#{@notification}.title",
      text: "notification.#{@notification}.text"
    }
  end

  # Trigger end of day actions
  def end_day
    @notification = nil
    self[:food] -= 4
    if self[:food].negative?
      self[:max_actions] -= 1
      self[:food] = 0
      @notification = 'not_enough_food'
    end

    if self[:max_actions].zero?
      @notification = 'too_late'
    end

    self[:days_left] -= 1
    if self[:days_left].zero?
      self[:max_actions] = 0
      @notification = 'too_weak'
    end
  end

  # Check if a certain resource is available
  def available?(resource)
    resource.nil? || self[resource].positive?
  end

  # Consume one item of a certain resource
  def consume(resource)
    self[resource] -= 1 unless resource.nil?
  end

  # Produce one item of a certain resource
  def produce(resource)
    if resource == 'ship'
      self[:max_actions] = 0
      @notification = 'victory'
      self[resource] += 1
    else
      self[resource] += 2
    end
  end

  # Add a random resource (from a list of possible choices)
  def add_one_random_resource
    resource = RANDOM_RESOURCES[rand(RANDOM_RESOURCES.length)]
    self[resource] += 1
  end
end
