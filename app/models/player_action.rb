class PlayerAction
  include ActiveModel::Model
  validates_with PlayerActionValidator
  attr_accessor :name, :tile_id, :victory

  # Parse action from name (by removing the prefix action.)
  def action
    name.split(/\./)[-1]
  end

  # End the day, consumes food and reset actions
  def end_day(status)
    status.end_day
    status.actions = status.max_actions
    status.save!
  end

  # Discover a new tile and make it visible
  def discover(status)
    tile = Tile.find(self.tile_id)
    raise Exception.new("Tile already visibile") if tile.visible?
    tile.visible = true
    tile.save!
    status.add_one_random_resource
    status.actions -= 1
    status.save!
  end

  # Gather food from a tile
  def gather_food(status)
    status.actions -= 1
    status.food += rand(1..3)
    status.save!
  end

  # Gather logs from a tile
  def gather_logs(status)
    status.actions -= 1
    status.logs += rand(1..3)
    status.save!
  end

  # Produce resources from a building
  def produce(status)
    tile = Tile.find(self.tile_id)
    building = tile.building
    if status.available?(building.consumes) && status.available?(building.consumes_second)
      status.consume(building.consumes)
      status.consume(building.consumes_second)
      status.produce(building.produces)
      status.actions -= 1
      status.save!
    end
  end

  def as_json_response(status, tile)
    if status.victory?
      return {
        player_action: self,
        status: status.reload,
        notification: status.notification
      }
    else
      return {
        player_action: self,
        status: status.reload,
        tiles: [tile.reload]
      }
    end
  end
end