# Super class for all models used in the JSON API, suppressing the timestamp field
class ApiModel < ApplicationRecord
  self.abstract_class = true

  def as_json(options = {})
    options[:except] = [] if options[:except].nil?
    options[:except] << :created_at << :updated_at
    super(options)
  end
end