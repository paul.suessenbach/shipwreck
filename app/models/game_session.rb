# A container class for the game session, binding session-specific items to a user
class GameSession < ApplicationRecord
  belongs_to :user
  has_many :tiles, -> { order 'id asc' }, dependent: :destroy
  has_one :status, dependent: :destroy

  validates :user_id, presence: true

  # Create a new game session for a user, initializing all the corresponding default values
  def GameSession.create_for_user(user)
    game_session = user.create_game_session!
    game_session.create_status!
    create_island game_session
    return game_session
  end

  private

  # Create a new random island for this game session
  def GameSession.create_island(game_session)
    # Create possible terrain tiles (always the same)
    terrains = %w[forest forest forest forest forest
                  mountains mountains mountains mountains mountains mountains
                  plains plains plains plains plains]
    terrains.shuffle!
    terrains.each_with_index { |terrain, index| create_tile(game_session, terrain, index) }
    # Set town hall to random position
    Building.create_town_hall!(game_session.tiles.sample)
  end

  # Create a single terrain tile and calculate its position on the board
  def GameSession.create_tile(game_session, terrain, index)
    column = index % 4 + 1
    row = index / 4 + 1
    game_session.tiles.create!(terrain: terrain, column: column, row: row)
  end
end
