# A blueprint for a player action
class ActionPlan < ApiModel
  validates :name, presence: true
end
