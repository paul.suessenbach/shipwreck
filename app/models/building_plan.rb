# A blueprint for a player-constructable building
class BuildingPlan < ApiModel
  validates :name, presence: true
  validates :costs_lumber, numericality: { greater_than_or_equal_to: 0 }
  validates :costs_stones, numericality: { greater_than_or_equal_to: 0 }
end
