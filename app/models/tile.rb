# A single filed on the island
class Tile < ApiModel
  belongs_to :game_session
  has_one :building, dependent: :destroy

  validates :game_session_id, presence: true

  def as_json(options = {})
    except_options = %i[game_session_id]
    except_options << :terrain unless visible?
    super(options.merge(except: except_options))
  end
end
