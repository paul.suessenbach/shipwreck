class PlayerActionValidator < ActiveModel::Validator
  def validate(record)
    if (action_plan_exists(record) && tile_exists(record))
      action_plan = ActionPlan.where('name = ?', record.name).take!
      tile = Tile.find(record.tile_id)

      if action_plan.possible_terrain != 'all' && action_plan.possible_terrain != tile.terrain
        record.errors[:tile_id] << "Action not allowed on #{tile.terrain}";
      end

      record.errors[:tile_id] << "Action not allowed on visible tile" if tile.visible? && action_plan.require_invisible?
    end
  end

  def action_plan_exists(record)
    exists = ActionPlan.exists?(['name = ?', record.name])
    record.errors[:name] << "Unknown action #{record.name}" unless exists
    return exists
  end

  def tile_exists(record)
    exists = Tile.exists?(record.tile_id)
    record.errors[:tile_id] << "Unknown tile with ID #{record.tile_id}" unless exists
    return exists
  end
end