class BuildingValidator < ActiveModel::Validator
  def validate(record)
    if tile_exists(record)
      tile = Tile.find(record.tile_id)
      record.errors[:tile_id] << (options[:message] || 'has already a building') unless tile.building.nil?
      return if record.town_hall?
      if building_plan_exists(record)
        building_plan = BuildingPlan.where('name = ?', record.name).take!
        if building_plan.possible_terrain != 'all' && building_plan.possible_terrain != tile.terrain
          record.errors[:tile_id] << "Building not allowed on #{tile.terrain}";
        end
      end
    end
  end

  def building_plan_exists(record)
    exists = BuildingPlan.exists?(['name = ?', record.name])
    record.errors[:name] << "Unknown building #{record.name}" unless exists
    return exists
  end

  def tile_exists(record)
    exists = Tile.exists?(record.tile_id)
    record.errors[:tile_id] << "Unknown tile with ID #{record.tile_id}" unless exists
    return exists
  end
end