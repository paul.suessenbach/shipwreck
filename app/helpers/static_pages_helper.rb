module StaticPagesHelper
  # Build the cost string of a building plan (for the manual page)
  def building_costs(building_plan)
    cost_str = ''
    cost_str << t('buildings.costs_lumber', count: building_plan.costs_lumber) if building_plan.costs_lumber.positive?
    cost_str << ' / ' if costs_stones_and_logs(building_plan)
    cost_str << t('buildings.costs_stones', count: building_plan.costs_stones) if building_plan.costs_stones.positive?
    cost_str
  end

  # Build the consumes and produces string of a building plan (for the manual page)
  def consumes(building_plan)
    cost_str = ''
    cost_str << t(building_plan.consumes) if building_plan.consumes
    cost_str << ' + ' if building_plan.consumes && building_plan.consumes_second
    cost_str << t(building_plan.consumes_second) if building_plan.consumes_second
    cost_str
  end

  # Build a string stating on which terrain this building can be constructed on (for the manual page)
  def terrain_restriction(building_plan)
    "#{t('manual.possible_terrain')} #{t("manual.terrain.#{building_plan.possible_terrain}")}"
  end

  private

  def costs_stones_and_logs(building_plan)
    building_plan.costs_lumber.positive? && building_plan.costs_stones.positive?
  end
end
