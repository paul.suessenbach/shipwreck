# Building plans
BuildingPlan.create!(
  name: 'buildings.mason',
  costs_lumber: 2,
  produces: 'stones',
  possible_terrain: 'mountains'
)
BuildingPlan.create!(
  name: 'buildings.sawmill',
  costs_lumber: 2,
  costs_stones: 2,
  consumes: 'logs',
  produces: 'lumber'
)
BuildingPlan.create!(
  name: 'buildings.well',
  costs_stones: 1,
  produces: 'water'
)
BuildingPlan.create!(
  name: 'buildings.ore_mine',
  costs_lumber: 3,
  consumes: 'food',
  consumes_second: 'water',
  produces: 'ore',
  possible_terrain: 'mountains'
)
BuildingPlan.create!(
  name: 'buildings.coal_mine',
  costs_lumber: 3,
  consumes: 'food',
  consumes_second: 'water',
  produces: 'coal',
  possible_terrain: 'mountains'
)
BuildingPlan.create!(
  name: 'buildings.foundry',
  costs_lumber: 2,
  costs_stones: 2,
  consumes: 'ore',
  consumes_second: 'coal',
  produces: 'iron'
)
BuildingPlan.create!(
  name: 'buildings.dockyard',
  costs_lumber: 2,
  costs_stones: 3,
  consumes: 'iron',
  consumes_second: 'lumber',
  produces: 'ship',
  possible_terrain: 'plains'
)

# Action plans
ActionPlan.create!(
  name: 'action.discover',
  require_invisible: true
)
ActionPlan.create!(
  name: 'action.gather_food',
  possible_terrain: 'plains'
)
ActionPlan.create!(
  name: 'action.gather_logs',
  possible_terrain: 'forest'
)
ActionPlan.create!(
  name: 'action.produce',
  require_building: true
)