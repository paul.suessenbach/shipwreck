# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_06_25_180925) do

  create_table "action_plans", force: :cascade do |t|
    t.string "name", null: false
    t.string "possible_terrain", default: "all"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "require_invisible", default: false
    t.boolean "require_building", default: false
  end

  create_table "building_plans", force: :cascade do |t|
    t.integer "costs_lumber", default: 0, null: false
    t.integer "costs_stones", default: 0, null: false
    t.string "name", null: false
    t.string "symbol", default: "fa-home", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "consumes"
    t.string "consumes_second"
    t.string "produces"
    t.string "possible_terrain", default: "all"
  end

  create_table "buildings", force: :cascade do |t|
    t.string "name"
    t.string "symbol"
    t.integer "tile_id"
    t.string "consumes"
    t.string "produces"
    t.integer "consumes_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "consumes_second"
    t.index ["tile_id"], name: "index_buildings_on_tile_id"
  end

  create_table "game_sessions", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "days_available", default: 10, null: false
    t.index ["user_id"], name: "index_game_sessions_on_user_id"
  end

  create_table "statuses", force: :cascade do |t|
    t.integer "game_session_id"
    t.integer "logs", default: 0, null: false
    t.integer "stones", default: 0, null: false
    t.integer "water", default: 0, null: false
    t.integer "lumber", default: 0, null: false
    t.integer "ore", default: 0, null: false
    t.integer "coal", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "iron", default: 0, null: false
    t.integer "actions", default: 0, null: false
    t.integer "food", default: 0, null: false
    t.integer "max_actions", default: 4, null: false
    t.integer "days_left", default: 10, null: false
    t.integer "ship", default: 0, null: false
    t.index ["game_session_id"], name: "index_statuses_on_game_session_id"
  end

  create_table "tiles", force: :cascade do |t|
    t.integer "game_session_id"
    t.string "terrain"
    t.integer "column"
    t.integer "row"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "visible", default: false
    t.index ["game_session_id"], name: "index_tiles_on_game_session_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
