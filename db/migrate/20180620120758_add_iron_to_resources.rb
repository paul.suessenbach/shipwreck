class AddIronToResources < ActiveRecord::Migration[5.2]
  def change
    add_column :resources, :iron, :integer, default: 0, null: false
  end
end
