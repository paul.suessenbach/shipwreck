class AddPossibleTerrainToBuildings < ActiveRecord::Migration[5.2]
  def change
    add_column :building_plans, :possible_terrain, :string, default: 'all'
  end
end
