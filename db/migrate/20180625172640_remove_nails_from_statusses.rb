class RemoveNailsFromStatusses < ActiveRecord::Migration[5.2]
  def change
    remove_column :statuses, :nails, :integer
  end
end
