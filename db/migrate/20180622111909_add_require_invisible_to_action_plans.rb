class AddRequireInvisibleToActionPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :action_plans, :require_invisible, :boolean, default: false
  end
end
