class RemodelBuildings < ActiveRecord::Migration[5.2]
  def change
    remove_column :buildings, :production_ticks, :integer
    remove_column :buildings, :last_production, :integer
    add_column :buildings, :consumes_second, :string

    add_column :building_plans, :consumes, :string
    add_column :building_plans, :consumes_second, :string
    add_column :building_plans, :produces, :string
  end
end
