class AddDaysCounter < ActiveRecord::Migration[5.2]
  def change
    add_column :statuses, :days_left, :integer, null: false, default: 10
    add_column :game_sessions, :days_available, :integer, null: false, default: 10
  end
end
