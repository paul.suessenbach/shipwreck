class CreateBuildingPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :building_plans do |t|
      t.integer :costs_lumber, default: 0, null: false
      t.integer :costs_stones, default: 0, null: false
      t.string :name, null: false
      t.string :symbol, default: "fa-home", null: false

      t.timestamps
    end
  end
end
