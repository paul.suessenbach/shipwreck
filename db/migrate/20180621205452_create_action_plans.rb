class CreateActionPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :action_plans do |t|
      t.string :name, null: false
      t.string :possible_terrain, default: 'all'

      t.timestamps
    end
  end
end
