class AddVisibleToTiles < ActiveRecord::Migration[5.2]
  def change
    add_column :tiles, :visible, :boolean, default: false
  end
end
