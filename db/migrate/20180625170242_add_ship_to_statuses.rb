class AddShipToStatuses < ActiveRecord::Migration[5.2]
  def change
    add_column :statuses, :ship, :integer, default: 0, null: false
  end
end
