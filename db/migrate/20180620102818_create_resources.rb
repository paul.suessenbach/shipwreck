class CreateResources < ActiveRecord::Migration[5.2]
  def change
    create_table :resources do |t|
      t.references :game_session, foreign_key: true, unique: true
      t.integer :logs, default: 0, null: false
      t.integer :stones, default: 0, null: false
      t.integer :water, default: 0, null: false
      t.integer :lumber, default: 0, null: false
      t.integer :beer, default: 0, null: false
      t.integer :flour, default: 0, null: false
      t.integer :crop, default: 0, null: false
      t.integer :bread, default: 0, null: false
      t.integer :ore, default: 0, null: false
      t.integer :coal, default: 0, null: false
      t.integer :nails, default: 0, null: false

      t.timestamps
    end
  end
end
