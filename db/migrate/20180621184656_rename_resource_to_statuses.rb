class RenameResourceToStatuses < ActiveRecord::Migration[5.2]
  def change
    rename_table :resources, :statuses
  end
end
