class CreateBuildings < ActiveRecord::Migration[5.2]
  def change
    create_table :buildings do |t|
      t.string :name
      t.string :symbol
      t.references :tile, foreign_key: true, unique: true
      t.string :consumes
      t.string :produces
      t.integer :consumes_count
      t.integer :production_ticks
      t.integer :last_production

      t.timestamps
    end
  end
end
