class RemodelStatusTable < ActiveRecord::Migration[5.2]
  def change
    add_column :statuses, :actions, :integer, default: 0, null: false
    add_column :statuses, :food, :integer, default: 0, null: false

    remove_column :statuses, :beer
    remove_column :statuses, :bread
    remove_column :statuses, :crop
    remove_column :statuses, :flour
  end
end
