class CreateTiles < ActiveRecord::Migration[5.2]
  def change
    create_table :tiles do |t|
      t.references :game_session, foreign_key: true
      t.string :terrain
      t.integer :column
      t.integer :row

      t.timestamps
    end
  end
end
