class CreateGameSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :game_sessions do |t|
      t.references :user, foreign_key: true, unique: true

      t.timestamps
    end
  end
end
