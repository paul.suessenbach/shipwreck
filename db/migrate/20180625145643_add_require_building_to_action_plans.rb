class AddRequireBuildingToActionPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :action_plans, :require_building, :boolean, default: false
  end
end
