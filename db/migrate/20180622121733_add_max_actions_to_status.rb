class AddMaxActionsToStatus < ActiveRecord::Migration[5.2]
  def change
    add_column :statuses, :max_actions, :integer, null: false, default: 4
  end
end
