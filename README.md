# Shipwreck

The following pages might be of interest for you:

* [German README](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/README-de.md)
* [Deployment on Heroku](https://damp-meadow-55369.herokuapp.com/)

Shipwreck is a simple survival game, where you are stranded on a small island and have
to build a ship to escape.

It's also a portfolio project, which explains the rather unsatisfying nature of the game. This
README focuses more on this aspect of the project. If you are interested in game itself, you
should read the [manual](https://damp-meadow-55369.herokuapp.com/manual).

# Structure

Shipwreck is a rather simple Ruby on Rails application. It has a small amount of static
pages, a basic authentication and user-management system and - at its core - a Rest API
that communicates with a Javascript front-end.

This REST API allow a simple game: The user can perform actions on the island and construct
buildings.

The main model is the 
[GameSession](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/models/game_session.rb),
which acts as container for all game-related data. Altough user and game session are in a 1:1
relationship, the game session is a separate model. On the one hand, for clearer separation
of data. On the other hand, it makes destroying sessions easier and removes all depended data
cleanly via ``depended: destroy``. Creating a game session also creates a whole set of
[Tile](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/models/tile.rb)-records,
which represent this island.

User-interaction after game-start is handled by POST requests to the _Buildings-_ resp.
_PlayerActionController_. Following "Never trust the front-end", all requests are validated.
As both PlayerAction and Building model have fields that are valid or invliad in combination
with other fields, both classes have special validators (found, of course, in
[app/validators](https://gitlab.com/paul.suessenbach/shipwreck/tree/master/app/validators)).

# Authentication

Authentication and user-management are complex, error-prone, but basically solved tasks.
Therefore, I did without a custom solution and went with the 
[Devise](https://github.com/plataformatec/devise) gem. The only big customization is a bit
more "Web 2.0" - login/-out are possible on every page via a dialog, instead of having
dedicated sites for them.

There is a single model
[(User)](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/models/user.rb),
which is completely Devise-generated code.

On the controller-side both
[SessionsController](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/controllers/users/sessions_controller.rb)
and
[RegistrationsController](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/controllers/users/registrations_controller.rb)
are custom, inheriting from the corresponding Devise classes and handling requests via REST.
Which means: The controllers expect and reply with JSON. Rails/Device request-from-forgery
mechanism expects a page reload after login, which is not the case here. There, the relevant
information gets packed with the JSON response.

# Internationalisation

The whole project is internationalized with the help of Rails I18n gem, both on the back-end
as on the front-end. It uses the HTTP_ACCEPT_LANGUAGE header as reference for the preferred
language. Parsing this header is done via the
[http_accept_language](https://github.com/iain/http_accept_language) gem.

As Javascript has no nice way of determing the preferred user language and to have the
information in sync on both back- and front-end, the project has a 
[Locale Controller](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/app/controllers/api/v1/locale_controller.rb).
This controller parses the HTTP header and passes the information back to the front-end.

To make sure all keys are available in all locale files, the project has an 
[appropiate test](https://gitlab.com/paul.suessenbach/shipwreck/blob/master/test/i18n_test.rb).

# Front-end

The front-end of Shipwreck is meant to simulate a proper JavaScript front-end of a
real-world application. On the _Play_ page, the page template inserts only a single div
(#board). The Javascript application then takes over and creates the whole application, taking to
the back-end Rails application via JSON Rest calls.

The front-end makes use of JQuery and Bootsstrap, but is not using any
sophisticated frameworks like Emer or AngularJS. Instead, it uses some basic of
controllers, views and makes use of Handlebar layouts.

# Tests

Even tough it's just a small Rails application, this project has of course tests. As most of
the logic is in the model, those are mainly unit tests for the models. There are a few
controller tests, though. There are _no_ tests for the Javascript front-end, as this is not
a demonstration of my Javascript-skills (spoiler: not that good). 